#include <algorithm>
#include <cstdlib>  // для использования exit()
#include <fstream>
#include <iostream>
#include <string>

#include "engine.hpp"

extern GLfloat lastFrame;
extern GLfloat deltaTime;

bool cube_is_clicked(Engine& engine, const Game_object& obj);
glm::vec3 get_similar_color(const glm::vec3& true_color,
                            float similarity_in_pecent);

void generate_colors(const glm::vec3& true_color,
                     std::vector<glm::vec3>& colors,
                     unsigned int& numer_of_right_cube) {
  float color_matching_coeff[] = {0.0f, 0.08f, 0.15f, 0.2f};
  unsigned int t = static_cast<unsigned int>(time(nullptr));
  srand(t);
  std::random_shuffle(std::begin(color_matching_coeff),
                      std::end(color_matching_coeff));

  colors.clear();
  for (float coeff : color_matching_coeff) {
    colors.push_back(get_similar_color(true_color, coeff));
  }

  for (unsigned int i = 0; i < 4; i++) {
    if (color_matching_coeff[i] == 0.0f) {
      numer_of_right_cube = i;
    }
  }
}

int main() {
  // Engine engine(480, 800);
  Engine engine(480, 720);
  engine.init();

  unsigned int numer_of_right_cube = 0;

  // ====================
  // Init SDL MIXER
  // ====================
  Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
  Mix_Music* menu_bg_music = Mix_LoadMUS("res/sound/menu.mp3");
  Mix_Music* game_bg_music = Mix_LoadMUS("res/sound/game.mp3");
  Mix_Chunk* sound_right_answ = Mix_LoadWAV("res/sound/click.wav");
  Mix_Chunk* sound_wrong_answ = Mix_LoadWAV("res/sound/wrong.wav");

  Mix_PlayMusic(menu_bg_music, -1);

  // ====================
  // TIME SLIDER
  // ====================
  Game_object time_slider;
  glm::vec3 slider_color = glm::vec3(0.25f, 0.25f, 0.25f);
  time_slider.posx = engine.get_screen_w() / 2;
  time_slider.posy = engine.get_screen_h();
  time_slider.scale_x = engine.get_screen_w();

  // ====================
  // CUBES COORDS
  // ====================
  Game_object cube, cube1, cube2, cube3, cube4;
  std::vector<Game_object> cubes;
  glm::vec3 true_color = engine.get_random_color();
  std::vector<glm::vec3> colors;
  generate_colors(true_color, colors, numer_of_right_cube);

  float cubes_scales = 120.0f;
  cube.posx = engine.get_screen_w() / 2;
  cube.posy = engine.get_screen_h() - engine.get_screen_h() / 3;

  cube1.posx = engine.get_screen_w() / 4;
  cube1.posy = engine.get_screen_h() / 3;
  cube1.scale_xy = cubes_scales;

  cube2.posx = engine.get_screen_w() / 4;
  cube2.posy = engine.get_screen_h() / 7;
  cube2.scale_xy = cubes_scales;

  cube3.posx = (engine.get_screen_w() / 8) * 6;
  cube3.posy = engine.get_screen_h() / 3;
  cube3.scale_xy = cubes_scales;

  cube4.posx = (engine.get_screen_w() / 8) * 6;
  cube4.posy = engine.get_screen_h() / 7;
  cube4.scale_xy = cubes_scales;

  cubes.push_back(cube1);
  cubes.push_back(cube2);
  cubes.push_back(cube3);
  cubes.push_back(cube4);
  // ====================
  // PHRASE "COLORS"
  // ====================
  Game_object letter_c;
  letter_c.load_vertices("res/cube_vertex.txt");
  letter_c.diffuseMap = engine.load_texture(letter_c, "res/font/c.png");
  letter_c.specularMap = engine.load_texture(letter_c, "res/font/c.png");

  Game_object letter_o1;
  letter_o1.load_vertices("res/cube_vertex.txt");
  letter_o1.diffuseMap = engine.load_texture(letter_o1, "res/font/o.png");
  letter_o1.specularMap = engine.load_texture(letter_o1, "res/font/o.png");

  Game_object letter_l;
  letter_l.load_vertices("res/cube_vertex.txt");
  letter_l.diffuseMap = engine.load_texture(letter_l, "res/font/l.png");
  letter_l.specularMap = engine.load_texture(letter_l, "res/font/l.png");

  Game_object letter_o2;
  letter_o2.load_vertices("res/cube_vertex.txt");
  letter_o2.diffuseMap = engine.load_texture(letter_o2, "res/font/o.png");
  letter_o2.specularMap = engine.load_texture(letter_o2, "res/font/o.png");

  Game_object letter_r;
  letter_r.load_vertices("res/cube_vertex.txt");
  letter_r.diffuseMap = engine.load_texture(letter_r, "res/font/r.png");
  letter_r.specularMap = engine.load_texture(letter_r, "res/font/r.png");

  Game_object letter_s;
  letter_s.load_vertices("res/cube_vertex.txt");
  letter_s.diffuseMap = engine.load_texture(letter_s, "res/font/s.png");
  letter_s.specularMap = engine.load_texture(letter_s, "res/font/s.png");
  //===================================================================*/
  Game_object cube_3d;
  cube_3d.load_vertices("res/cube_vertex.txt");
  glm::vec3 cube_3d_color = engine.get_random_color();

  Sprite play_btn;
  play_btn.diffuseMap =
      engine.load_sprite_img(play_btn, "res/gui/play_btn.png");

  Sprite play_btn_prsd;
  play_btn_prsd.diffuseMap =
      engine.load_sprite_img(play_btn, "res/gui/play_btn_prsd.png");

  // ====================
  // GUI PARAMETERS (play button, etc)
  // ====================
  float btn_scale = engine.get_screen_w() / 6;
  float btn_start_posx = engine.get_screen_w() / 2;
  float btn_start_posy = engine.get_screen_h() / 5;

  int score = 0;
  int maximum_score = 0;
  std::string max_score = "max score: ";
  const char* record_path = "res/max_score.txt";

  std::ofstream outf;
  std::ifstream inf;
  inf.open(record_path, std::ios::out | std::ios::binary);
  if (!inf) {
    outf.open(record_path, std::ios::in | std::ios::binary);
    outf << "0";
    outf.close();
  } else {
    std::string tmp;
    inf >> tmp;
    max_score += tmp;
  }
  inf.close();

  Sprite score_img;

  char const* p_char_score = max_score.c_str();
  score_img.diffuseMap = engine.get_texture_from_text(p_char_score);
  score_img.translate(engine.get_screen_w() / 2,
                      btn_start_posy - play_btn.height / 2 - 15, 0.0f);
  score_img.scale(100, 30, 1.0f);

  bool show_menu = true;
  bool startGame = false;
  bool play_btn_was_pressed = false;

  // ====================
  // Bind
  // ====================
  engine.bind_sprites(play_btn);
  engine.bind_game_object(cube_3d);
  engine.add_vert_to_VBO_buffer(letter_o1.vertices);
  engine.bind_letters();

  // ====================
  // GAME LOOP
  // ====================
  bool run = true;
  engine.camera.camera_on = false;
  float animation_time = 70;

  while (run) {
    engine.calc_delta_time();
    engine.read_input(run);
    engine.do_movement(run);
    engine.clear_display();

    // ====================
    // READ INPUT
    // ====================
    // IN MAIN MENU
    if (engine.left_mouse_click && !startGame) {
      if (engine.cursor_xpos > (btn_start_posx - play_btn.width / 2) &&
          engine.cursor_xpos < btn_start_posx + play_btn.width / 2 &&
          engine.cursor_ypos >
              (engine.get_screen_h() - btn_start_posy) - play_btn.height / 2 &&
          engine.cursor_ypos <
              (engine.get_screen_h() - btn_start_posy) + play_btn.height / 2) {
        play_btn_was_pressed = true;
        animation_time = 50;
      } else {
        if (show_menu && !play_btn_was_pressed) {
          Mix_PlayChannel(-1, sound_right_answ, 0);
          cube_3d_color = engine.get_random_color();
          play_btn.translate(btn_start_posx, btn_start_posy, 0.0f);
          play_btn.scale(btn_scale);
          engine.draw_sprite(play_btn);
        }
      }
    }

    // IN GAME
    if (engine.left_mouse_click && startGame) {
      engine.left_mouse_click = false;
      for (unsigned long i = 0; i < cubes.size(); i++) {
        if (cube_is_clicked(engine, cubes[i])) {
          // std::cout << "cube #"<< i << " was clicked " <<
          // std::endl;
          if (i == numer_of_right_cube) {
            Mix_PlayChannel(-1, sound_right_answ, 0);
            score++;
            true_color = engine.get_random_color();
            generate_colors(true_color, colors, numer_of_right_cube);
            time_slider.scale_x = engine.get_screen_w();

            std::string string_score = std::to_string(score);
            char const* p_char_score = string_score.c_str();
            score_img.diffuseMap = engine.get_texture_from_text(p_char_score);
            score_img.translate(engine.get_screen_w() / 2, cube.posy + 170,
                                0.0f);
            score_img.scale(20, 60, 1.0f);
          } else {
            // std::cout << "Wrong cube" << std::endl;
            Mix_PlayChannel(-1, sound_wrong_answ, 0);
            if (score > maximum_score) {
              maximum_score = score;
              outf.open(record_path, std::ios::binary);
              outf << maximum_score;
              outf.close();

              inf.open(record_path, std::ios::binary);
              std::string tmp;
              inf >> tmp;
              max_score = "max score: " + tmp;
              inf.close();
            }

            char const* p_char_score = max_score.c_str();
            score_img.diffuseMap = engine.get_texture_from_text(p_char_score);
            score_img.translate(engine.get_screen_w() / 2,
                                btn_start_posy - play_btn.height / 2 - 15,
                                0.0f);
            score_img.scale(100, 30, 1.0f);

            startGame = false;
            show_menu = true;
            engine.left_mouse_click = false;
            Mix_PlayMusic(menu_bg_music, -1);
          }
        }
      }
    }

    engine.validate_opengl_errors();

    // ====================
    // Set Letters positions
    // ====================
    if (show_menu) {
      letter_c.scale(0.5f);
      letter_c.translate(-3.5f, 6.0f, -15.0f);
      float angle = sin(engine.get_time() * 0.001f) * 10.0f;
      letter_c.rotate(angle, 1.0f, 0.0f, 0.0f);

      letter_o1.scale(0.5f);
      letter_o1.translate(-2.1f, 6.0f, -15.0f);
      angle = engine.get_time() * 0.001f * 35.0f;
      letter_o1.rotate(angle, 0.0f, 1.0f, 0.0f);

      letter_l.scale(0.5f);
      letter_l.translate(-0.7f, 6.0f, -15.0f);
      letter_l.rotate(180, 1.0f, 0.0f, 0.0f);
      angle = sin(engine.get_time() * 0.002f) * 10.0f;
      letter_l.rotate(angle, 0.0f, 1.0f, 0.0f);

      letter_o2.scale(0.3f);
      letter_o2.translate(0.7f, 6.0f, -15.0f);
      angle = engine.get_time() * 0.001f * 40.0f;
      letter_o2.rotate(angle, 1.0f, 0.0f, 0.0f);

      letter_r.scale(0.3f);
      letter_r.translate(2.1f, 6.0f, -15.0f);
      letter_r.rotate(-90, 1.0f, 0.0f, 0.0f);
      angle = sin(engine.get_time() * 0.0015f + 60) * 15.0f;
      letter_r.rotate(angle, 0.0f, 1.0f, 0.0f);

      letter_s.scale(0.3f);
      letter_s.translate(3.5f, 6.0f, -15.0f);
      letter_s.rotate(-90, 1.0f, 0.0f, 0.0f);
      angle = sin(engine.get_time() * 0.001f) * 10.0f;
      letter_s.rotate(angle, 0.0f, 1.0f, 0.0f);
    }

    engine.validate_opengl_errors();

    // ====================
    // DRAW MAIN MENU
    // ====================
    if (show_menu) {
      engine.draw_letter(letter_c);
      engine.draw_letter(letter_o1);
      engine.draw_letter(letter_l);
      engine.draw_letter(letter_o2);
      engine.draw_letter(letter_r);
      engine.draw_letter(letter_s);

      cube_3d.scale(1);
      float y_pos = sin(engine.get_time() * 0.001f) * 0.2f + 0.5f;
      cube_3d.translate(0.0f, y_pos, -2.0f);
      float angle = engine.get_time() * 0.001f * 50.0f;
      cube_3d.rotate(angle, 0.0f, -1.2f, -0.3f);
      engine.draw_game_obj(cube_3d, cube_3d_color);

      if (!play_btn_was_pressed) {
        play_btn.translate(btn_start_posx, btn_start_posy, 0.0f);
        play_btn.scale(btn_scale);
        engine.draw_sprite(play_btn);
      }
      engine.draw_sprite(score_img);
    }

    engine.validate_opengl_errors();

    // ====================
    // DRAW GAME
    // ====================
    if (startGame) {
      cube.translate(cube.posx, cube.posy, 0.0f);
      cube.scale(220.0f);
      engine.draw_game_obj_ortho(cube, true_color);

      for (unsigned long i = 0; i < cubes.size(); i++) {
        cubes[i].translate(cubes[i].posx, cubes[i].posy, 0.0f);
        cubes[i].scale(cubes_scales);
        engine.draw_game_obj_ortho(cubes[i], colors[i]);
      }

      time_slider.translate(time_slider.posx, time_slider.posy, 0.0f);

      time_slider.scale_x -= deltaTime * 0.2f;
      if (time_slider.scale_x <= 0) {
        // std::cout << "Game over." << std::endl;
        // run = false;
        engine.deleteTexture(engine.font_texture);

        Mix_PlayMusic(menu_bg_music, -1);

        if (score > maximum_score) {
          maximum_score = score;
          outf.open("max_score.txt", std::ios::binary);
          outf << maximum_score;
          outf.close();

          inf.open("max_score.txt", std::ios::binary);
          std::string tmp;
          inf >> tmp;
          max_score = "max score: " + tmp;
          inf.close();
        }
        char const* p_char_score = max_score.c_str();
        score_img.diffuseMap = engine.get_texture_from_text(p_char_score);
        score_img.translate(engine.get_screen_w() / 2,
                            btn_start_posy - play_btn.height / 2 - 15, 0.0f);
        score_img.scale(100, 30, 1.0f);

        startGame = false;
        show_menu = true;
      }
      time_slider.scale(time_slider.scale_x, 40.0f, 1.0f);
      engine.draw_game_obj_ortho(time_slider, slider_color);

      engine.draw_sprite(score_img);
    }

    engine.validate_opengl_errors();

    // ANIMATION PRESSED PLAY BTN
    if (play_btn_was_pressed) {
      play_btn_prsd.translate(btn_start_posx, btn_start_posy, 0.0f);
      play_btn_prsd.scale(btn_scale);
      engine.draw_sprite(play_btn_prsd);

      animation_time -= deltaTime;
      if (animation_time < 0) {
        // Mix_HaltMusic();

        std::string string_score = std::to_string(score);
        char const* p_char_score = string_score.c_str();
        score_img.diffuseMap = engine.get_texture_from_text(p_char_score);
        score_img.translate(engine.get_screen_w() / 2, cube.posy + 170, 0.0f);
        score_img.scale(20, 60, 1.0f);

        play_btn_was_pressed = false;
        startGame = true;
        score = 0;
        time_slider.scale_x = engine.get_screen_w();
        show_menu = false;
        // Mix_PlayMusic(game_bg_music, -1);
        Mix_PlayChannel(-1, sound_right_answ, 0);
      }
    }

    engine.validate_opengl_errors();

    // Swap the screen buffers
    engine.swap_buffers();
  }

  Mix_FreeChunk(sound_right_answ);
  Mix_FreeChunk(sound_wrong_answ);
  Mix_FreeMusic(menu_bg_music);
  Mix_FreeMusic(game_bg_music);

  engine.deleteTexture(engine.font_texture);

  engine.deleteTexture(play_btn.textureID);
  engine.deleteTexture(play_btn.diffuseMap);
  engine.deleteTexture(play_btn.specularMap);

  engine.deleteTexture(play_btn_prsd.textureID);
  engine.deleteTexture(play_btn_prsd.diffuseMap);
  engine.deleteTexture(play_btn_prsd.specularMap);

  engine.deleteTexture(letter_c.textureID);
  engine.deleteTexture(letter_c.diffuseMap);
  engine.deleteTexture(letter_c.specularMap);

  engine.deleteTexture(letter_o1.textureID);
  engine.deleteTexture(letter_o1.diffuseMap);
  engine.deleteTexture(letter_o1.specularMap);
  engine.deleteTexture(letter_l.diffuseMap);
  engine.deleteTexture(letter_l.specularMap);
  engine.deleteTexture(letter_o2.diffuseMap);
  engine.deleteTexture(letter_o2.specularMap);
  engine.deleteTexture(letter_r.diffuseMap);
  engine.deleteTexture(letter_r.specularMap);
  engine.deleteTexture(letter_s.diffuseMap);
  engine.deleteTexture(letter_s.specularMap);

  glad_glDeleteShader(engine.shader_gui->ID);
  glad_glDeleteShader(engine.shader_font_img->ID);
  glad_glDeleteShader(engine.shader_lighting->ID);
  glad_glDeleteShader(engine.shader_simple_color->ID);
  engine.quit();

  return 0;
}

glm::vec3 get_similar_color(const glm::vec3& true_color,
                            float similarity_in_pecent) {
  float r = true_color.r + similarity_in_pecent;
  float g = true_color.g + similarity_in_pecent;
  float b = true_color.b + similarity_in_pecent;
  glm::vec3 color = glm::vec3(r, g, b);
  return color;
}

bool cube_is_clicked(Engine& engine, const Game_object& obj) {
  if (engine.cursor_xpos > (obj.posx - obj.scale_xy * 0.5) &&
      engine.cursor_xpos < (obj.posx + obj.scale_xy * 0.5) &&
      engine.cursor_ypos >
          (engine.get_screen_h() - obj.posy - obj.scale_xy * 0.5f) &&
      engine.cursor_ypos <
          (engine.get_screen_h() - obj.posy + obj.scale_xy * 0.5f)) {
    return true;
  } else {
    return false;
  }
}
