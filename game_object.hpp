#pragma once

#include <fstream>
#include <string>
#include <vector>

#include "glad/glad.h"  // OpenGL ES 3.0
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

class Game_object {
 public:
  Game_object();
  int load_vertices(const char* path);
  void rotate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
  void translate(GLfloat x, GLfloat y, GLfloat z);
  void scale(GLfloat x, GLfloat y, GLfloat z);
  void scale(GLfloat scale);

  std::vector<GLfloat> vertices;
  std::vector<GLfloat> indices;
  int width, height, nrComponents;

  unsigned char* sprite = nullptr;
  GLuint textureID;
  GLuint vbo;
  unsigned int diffuseMap = 0;
  unsigned int specularMap = 0;
  glm::mat4 model;
  glm::mat4 view;
  glm::mat4 projection;

  GLfloat posx = 0, posy = 0;
  GLfloat scale_x = 1;
  GLfloat scale_y = 1;
  GLfloat scale_xy = 1;
};
