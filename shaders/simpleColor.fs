#version 300 es
//#version 330 core
precision mediump float;

out vec4 FragColor;

struct Material {
    //vec3 ambient;
    vec3  diffuse;
    vec3 specular;
    float shininess;
};

struct Light {
    // vec3 position; // Не требуется для направленного источника.
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;

uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{
    // ambient
    vec3 ambient = material.diffuse;

    // diffuse
    vec3 diffuse = material.diffuse;

    vec3 result = diffuse;
    FragColor = vec4(result, 1.0);
}
