#version 300 es
//#version 330 core
precision mediump float;

out vec4 FragColor;

struct Material {
    sampler2D  diffuse;
};


in vec2 TexCoords;
in vec3 FragPos;

uniform Material material;

void main()
{
    FragColor = texture(material.diffuse, TexCoords);
}
