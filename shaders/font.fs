#version 300 es
precision mediump float;

in vec2 texCoord;
in vec4 fragColor;

out vec4 finalColor;
uniform sampler2D myTextureSampler;

void main()
{
    finalColor = texture( myTextureSampler, texCoord ) * fragColor;
}
