#include "engine.hpp"

#include <algorithm>
#include <iostream>

#include "cassert"

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

GLfloat deltaTime = 0.0f; // Время, прошедшее между последним
                          // и текущим кадром
GLfloat lastFrame = 0.0f; // Время вывода последнего кадра

Sprite::Sprite()
{
    vertices = {
        // positions             // normals           // texture coords
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Top Right
        1.0f,  -1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // Bottom Right
        1.0f,  1.0f,  0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // Bottom Left

        1.0f,  1.0f,  0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // Top Left
        -1.0f, 1.0f,  0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, // Top Right
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f  // Bottom Left
    };

    glGenBuffers(1, &vbo);
    model = glm::mat4(1.0f);
};

void Sprite::translate(GLfloat x, GLfloat y, GLfloat z)
{
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(x, y, z));
};

void Sprite::scale(GLfloat x, GLfloat y, GLfloat z)
{
    model = glm::scale(model, glm::vec3(x, y, z));
};

void Sprite::scale(GLfloat scale)
{
    model = glm::scale(model, glm::vec3(scale, scale, scale));
}

Engine::Engine(int w, int h)
{
    screen_width  = w;
    screen_height = h;

    // camera
    lastX  = screen_width / 2;
    lastY  = screen_height / 2;
    camera = (glm::vec3(0.0f, 0.0f, 3.0f));
    //    camera.yaw = -180.0f;
    //    camera.pitch = -61.0f;
    camera.update_camera_vectors();

    // lighting
    lightPos = glm::vec3(.5f, 0.5f, 2.0f);
};

int Engine::init()
{
    using namespace std;
    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);
    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        cerr << "warning: SDL2 compiled and linked version mismatch: "
             << &compiled << " " << &linked << endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
    {
        cerr << "error: can't init: " << SDL_GetError() << endl;
        system("pause");
        return 1;
    }

    window = SDL_CreateWindow("Colors", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << "error: can't create window: " << SDL_GetError() << std::endl;
        system("pause");
        return 1;
    }

    int gl_major_ver = 3;
    int gl_minor_ver = 0;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }

    assert(gl_context != nullptr);

    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    if (gl_major_ver < 3)
    {
        clog << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need openg version at least: 3.0\n"
             << flush;
        throw runtime_error("opengl version too low");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        cerr << "error: failed to initialize glad" << endl;
    }
    // ********* Build and compile our shader program ********* //
    shader_gui      = new Shader("shaders/gui.vs", "shaders/gui.fs");
    shader_lighting = new Shader("shaders/material.vs", "shaders/material.fs");
    shader_font_img = new Shader("shaders/font_img.vs", "shaders/font_img.fs");
    shader_simple_color =
        new Shader("shaders/simpleColor.vs", "shaders/simpleColor.fs");

    print_opengl_version();

    // ====================
    // Setup OpenGL options
    // ====================
    glEnable(GL_DEPTH_TEST);
    // SDL_ShowCursor(SDL_FALSE); // HIDE CURSOR
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    TTF_Init();
    font       = TTF_OpenFont("res/font/font.ttf", 60);
    font_color = { 0, 0, 0, 255 };

    // VAO and VBO
    glGenVertexArrays(1, &light_VAO);
    glGenBuffers(1, &light_VBO);

    glGenVertexArrays(1, &game_obj_VAO);
    glGenBuffers(1, &game_obj_VBO);

    glGenVertexArrays(1, &gui_VAO);
    glGenBuffers(1, &gui_VBO);

    glGenVertexArrays(1, &sprite_VAO);
    glGenBuffers(1, &sprite_VBO);

    validate_opengl_errors();
    return 0;
};

void Engine::swap_buffers()
{
    SDL_GL_SwapWindow(window);
    validate_opengl_errors();
}

void Engine::bind_sprites(Sprite& sprite)
{
    glBindVertexArray(sprite_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, sprite.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(sprite.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &sprite.vertices.front(), GL_STREAM_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Texture vertex attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // game_obj_VAO
};

unsigned int Engine::load_sprite_img(Sprite& sprite, const char* path)
{
    // unsigned int textureID;
    glGenTextures(1, &sprite.textureID);

    sprite.image =
        stbi_load(path, &sprite.width, &sprite.height, &sprite.nrComponents, 0);

    if (sprite.image)
    {
        GLenum format = 0;
        if (sprite.nrComponents == 1)
            format = GL_RED;
        else if (sprite.nrComponents == 3)
            format = GL_RGB;
        else if (sprite.nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, sprite.textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, sprite.width, sprite.height, 0,
                     format, GL_UNSIGNED_BYTE, sprite.image);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(sprite.image);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(sprite.image);
    }
    return sprite.textureID;
};

void Engine::draw_sprite(Sprite& sprite)
{
    // be sure to activate shader when setting uniforms/drawing objects
    shader_gui->use();
    shader_gui->setInt("material.diffuse", 0);

    shader_gui->setVec3("viewPos", camera.position);

    // material properties
    shader_gui->setFloat("material.shininess", 64.0f);

    // view/projection transformations
    sprite.view = camera.GetViewMatrix();
    //  sprite.projection = glm::perspective(
    //      camera.zoom, screen_width / static_cast<float>(screen_height), 0.1f,
    //      100.0f);
    sprite.projection =
        glm::ortho(0.0f, static_cast<float>(screen_width), 0.0f,
                   static_cast<float>(screen_height), -10.0f, 100.0f);

    shader_gui->setMat4("projection", sprite.projection);
    shader_gui->setMat4("view", sprite.view);

    // world transformation
    shader_gui->setMat4("model", sprite.model);

    // bind diffuse map
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, sprite.diffuseMap);
    // bind specular map
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, sprite.specularMap);

    // Draw the container (using container's vertex attributes)
    glBindVertexArray(sprite_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
};

void Engine::bind_game_object(Game_object& game_obj)
{
    glBindVertexArray(game_obj_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, game_obj.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(game_obj.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &game_obj.vertices.front(), GL_STREAM_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0); // game_obj_VAO
};

unsigned int Engine::load_texture(Game_object& game_obj, const char* path)
{
    // unsigned int textureID;
    glGenTextures(1, &game_obj.textureID);

    game_obj.sprite = stbi_load(path, &game_obj.width, &game_obj.height,
                                &game_obj.nrComponents, 0);

    if (game_obj.sprite)
    {
        GLenum format = 0;
        if (game_obj.nrComponents == 1)
            format = GL_RED;
        else if (game_obj.nrComponents == 3)
            format = GL_RGB;
        else if (game_obj.nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, game_obj.textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, game_obj.width, game_obj.height,
                     0, format, GL_UNSIGNED_BYTE, game_obj.sprite);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(game_obj.sprite);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(game_obj.sprite);
    }
    return game_obj.textureID;
};

void Engine::draw_game_obj(Game_object& game_obj, glm::vec3& color)
{
    // be sure to activate shader when setting uniforms/drawing objects
    shader_lighting->use();

    // lightingShader->setVec3("light.position", lightPos);
    shader_lighting->setVec3("light.direction", -0.2f, -1.0f, -0.3f);
    shader_lighting->setVec3("viewPos", camera.position);

    // light properties
    // glm::vec3 lightColor;
    shader_lighting->setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    shader_lighting->setVec3("light.diffuse", 1.0f, 1.0f, 1.0f);
    shader_lighting->setVec3("light.specular", 1.0f, 1.0f, 1.0f);

    // material properties

    shader_lighting->setVec3("material.diffuse", color);
    shader_lighting->setVec3("material.specular", 0.5f, 0.5f, 0.5f);
    shader_lighting->setFloat("material.shininess", 64.0f);

    // view/projection transformations
    game_obj.view       = camera.GetViewMatrix();
    game_obj.projection = glm::perspective(
        camera.zoom, screen_width / static_cast<float>(screen_height), 0.1f,
        100.0f);

    shader_lighting->setMat4("projection", game_obj.projection);
    shader_lighting->setMat4("view", game_obj.view);

    // world transformation
    shader_lighting->setMat4("model", game_obj.model);

    // Draw the container (using container's vertex attributes)
    glBindVertexArray(gui_VAO);
    glDrawArrays(GL_TRIANGLES, 0, game_obj.vertices.size());
    glBindVertexArray(0);
};

void Engine::draw_game_obj_ortho(Game_object& game_obj, glm::vec3& color)
{
    // be sure to activate shader when setting uniforms/drawing objects
    shader_simple_color->use();

    // material properties
    shader_simple_color->setVec3("material.diffuse", color);

    // view/projection transformations
    game_obj.view = camera.GetViewMatrix();
    game_obj.projection =
        glm::ortho(0.0f, static_cast<float>(screen_width), 0.0f,
                   static_cast<float>(screen_height), -1.0f, 1000.0f);

    shader_simple_color->setMat4("projection", game_obj.projection);
    shader_simple_color->setMat4("view", game_obj.view);

    // world transformation
    shader_simple_color->setMat4("model", game_obj.model);

    // Draw the container (using container's vertex attributes)
    glBindVertexArray(game_obj_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
};

void Engine::bind_letters()
{
    glBindVertexArray(gui_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, gui_VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(gui_vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &gui_vertices.front(), GL_STREAM_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // Texture vertex attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0);
};

void Engine::draw_letter(Game_object& game_obj)
{
    // be sure to activate shader when setting uniforms/drawing objects
    shader_font_img->use();
    shader_font_img->setVec3("light.direction", -0.2f, -1.0f, -0.3f);
    shader_font_img->setVec3("viewPos", camera.position);

    // light properties
    shader_font_img->setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    shader_font_img->setVec3("light.diffuse", 0.5f, 0.5f, 0.5f);
    shader_font_img->setVec3("light.specular", 1.0f, 1.0f, 1.0f);

    // material properties
    shader_font_img->setFloat("material.shininess", 32.0f);

    // view/projection transformations
    game_obj.view       = camera.GetViewMatrix();
    game_obj.projection = glm::perspective(
        camera.zoom, screen_width / static_cast<float>(screen_height), 0.1f,
        100.0f);

    shader_font_img->setMat4("projection", game_obj.projection);
    shader_font_img->setMat4("view", game_obj.view);

    // world transformation
    shader_font_img->setMat4("model", game_obj.model);

    // bind diffuse map
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, game_obj.diffuseMap);
    // bind specular map
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, game_obj.specularMap);

    // render the cube
    glBindVertexArray(gui_VAO);
    GLint num_of_vert = static_cast<GLint>(gui_vertices.size());
    glDrawArrays(GL_TRIANGLES, 0, num_of_vert);
    glBindVertexArray(0);
};

int Engine::quit()
{
    TTF_CloseFont(font);
    font = nullptr;
    TTF_Quit();

    Mix_CloseAudio();

    glDeleteBuffers(1, &game_obj_VAO);
    glDeleteBuffers(1, &game_obj_VBO);

    glDeleteBuffers(1, &gui_VAO);
    glDeleteBuffers(1, &gui_VBO);

    glDeleteBuffers(1, &light_VAO);
    glDeleteBuffers(1, &light_VBO);

    if (shader_gui != nullptr)
    {

        glDeleteProgram(shader_gui->ID);
        shader_gui = nullptr;
    }
    if (shader_font_img != nullptr)
    {
        glDeleteProgram(shader_font_img->ID);
        shader_font_img = nullptr;
    }
    if (shader_lighting != nullptr)
    {
        glDeleteProgram(shader_lighting->ID);
        shader_lighting = nullptr;
    }
    if (shader_simple_color != nullptr)
    {
        glDeleteProgram(shader_simple_color->ID);
        shader_simple_color = nullptr;
    }
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
};

void Engine::clear_display()
{
    glClearColor(1.0f, 1.0f, .85f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
};

Engine::~Engine()
{
    this->quit();
};

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
            case GL_INVALID_ENUM:
                message =
                    "invalid enum passed to GL function (GL_INVALID_ENUM)";
                break;
            case GL_INVALID_VALUE:
                message = "invalid parameter passed to GL function "
                          "(GL_INVALID_VALUE)";
                break;
            case GL_INVALID_OPERATION:
                message = "cannot execute some of GL functions in current "
                          "state (GL_INVALID_OPERATION)";
                break;
            case GL_OUT_OF_MEMORY:
                message = "no enough memory to execute GL function "
                          "(GL_OUT_OF_MEMORY)";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                message = "invalid framebuffer operation "
                          "(GL_INVALID_FRAMEBUFFER_OPERATION)";
                break;
            default:
                message = "error in some GL extension (framebuffers, "
                          "shaders, etc)";
                break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
};

void Engine::print_opengl_version()
{
    using namespace std;
    string version     = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info =
        reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    clog << "OpenGL version: " << version << endl;
    clog << "OpenGL vendor: " << vendor_info << endl;
    // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};

GLfloat Engine::calc_delta_time()
{
    GLfloat currentFrame = get_time();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    return deltaTime;
}

// ====================
// READ INPUT
// ====================
void Engine::read_input(bool& run)
{
    while (SDL_PollEvent(&e) != 0)
    {
        if (e.type == SDL_QUIT)
        {
            run = false;
        }

        // KEYDOWN
        if (e.type == SDL_KEYDOWN)
        {
            keys[e.key.keysym.sym] = true;
        }
        // KEYUP
        if (e.type == SDL_KEYUP)
        {
            keys[e.key.keysym.sym] = false;
        }
        // MOUSE CLICK DOWN
        if (e.type == SDL_MOUSEBUTTONDOWN)
        {
            // keys[e.button.button] = true;
            left_mouse_click = true;
        }
        // MOUSE CLICK UP
        if (e.type == SDL_MOUSEBUTTONUP)
        {
            // keys[e.button.button] = false;
            left_mouse_click = false;
        }
        // MOUSE WHEEL
        if (e.type == SDL_MOUSEWHEEL)
        {
            GLfloat offset = 1;
            if (e.wheel.y > 0) // scroll up
            {
                if (camera.zoom < 45)
                {
                    camera.zoom += offset;
                }
                else
                {
                    camera.zoom = 45;
                }
            }
            else if (e.wheel.y < 0) // scroll down
            {
                if (camera.zoom > 1)
                {
                    camera.zoom -= offset;
                }
                else
                {
                    camera.zoom = 1;
                }
            }
        }

        // MOUSE MOUTION
        if (e.type == SDL_MOUSEMOTION)
        {
            SDL_GetMouseState(&cursor_xpos, &cursor_ypos);
            if (first_mouse)
            {
                lastX       = cursor_xpos;
                lastY       = cursor_ypos;
                first_mouse = false;
            }

            GLfloat xoffset = cursor_xpos - lastX;
            GLfloat yoffset =
                lastY - cursor_ypos; // Reversed since y-coordinates go
                                     // from bottom to left
            lastX = cursor_xpos;
            lastY = cursor_ypos;

            xoffset *= camera.mouse_sensitivity;
            yoffset *= camera.mouse_sensitivity;

            camera.yaw += xoffset;
            camera.pitch += yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't
            // get flipped
            if (camera.pitch > 89.0f)
                camera.pitch = 89.0f;
            if (camera.pitch < -89.0f)
                camera.pitch = -89.0f;

            if (camera.camera_on)
            {
                camera.update_camera_vectors();
            }
        }
    }
}

void Engine::do_movement(bool& run)
{
    float velocity = camera.movement_speed * deltaTime;
    if (keys[SDLK_ESCAPE])
        run = false;
    if (keys[SDLK_w])
    {
        camera.position += camera.front * velocity;
    }
    if (keys[SDLK_s])
    {
        camera.position -= camera.front * velocity;
    }
    if (keys[SDLK_a])
    {
        camera.position -= camera.right * velocity;
    }
    if (keys[SDLK_d])
    {
        camera.position += camera.right * velocity;
    }
    // Mouse click
    //    if (keys[SDL_BUTTON_LEFT])
    //    {
    //        left_mouse_click = true;
    //    }
    //    if (!keys[SDL_BUTTON_LEFT])
    //    {
    //        left_mouse_click = false;
    //    }
}

unsigned int Engine::get_texture_from_text(const char* text)
{

    text_surface = TTF_RenderUTF8_Blended(font, text, font_color);

    // Your format checker
    //    GLenum format =
    //        (text_surface->format->BytesPerPixel == 3) ? GL_RGB : GL_RGBA;

    // Create OpenGL Texture
    glGenTextures(1, &font_texture);
    glBindTexture(GL_TEXTURE_2D, font_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, text_surface->w, text_surface->h, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, text_surface->pixels);

    // Set Some basic parameters
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Set up Sampler
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, font_texture);

    SDL_FreeSurface(text_surface);

    return font_texture;
};
